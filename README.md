# Maze Solver

Dies ist ein kleines Algorithmus-Projekt entwickelt in ReactJS, um einen zufällig generierten Irrenweg zu lösen.

## Startverfahren

Mit der folgenden Anweisung kann die Applikation ausgeführt werden:

### `yarn start`

Sofern nichts schief geraten ist, sollte die Webseite in einem Browserfenster erscheinen.\
Ansonsten können Sie durch diese URL die Ansicht selbst auffordern:

[http://localhost:5173](http://localhost:5173)

## Algorithmus im Einsatz

Name: `Algorithmus von Trémaux`

Beschreibung:

Dieser Ansatz verwendet Markierungen vor und nach Ecken im Irrgarten, um Wegwiederholungen zu vermeiden.

Voraussetzungen:

Einen Irrgarten mit eindeutigen Passagen. Das Feld and sich muss dem Weglöser nicht bekannt sein.

Überprüfung der Korrektheit:

Unter anderem ist die Korrektheit dieses Algorithmus, selbst von der Problematik leicht zu bestimmen.
Korrekt ist, wenn die Spielfigur, bei Möglichkeit, erfolgreich aus dem Irrgarten geflohen ist.

Effizienz:

In den Umständen, in welchem das Spielfeld unbekannt ist, offeriert Trémaux eines der effizientesten Wegfindungsmethoden.
Ansonsten gibt es seit der Entwicklung des Algorithmus optimalere Lösungen.

Komplexität:

Um ehrlich zu sein ist meine Implementation davon suboptimal, da es redundante Codeausschnitte enthält und viel komplexer als nötig geschrieben ist.
