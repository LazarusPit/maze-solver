import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import MazeView from "./components/MazeView";
import {createTheme, Grid2} from "@mui/material";
import {ThemeProvider} from "@mui/styles";

const theme = createTheme();

function App() {

    return (
        <ThemeProvider theme={theme}>
            <Grid2 container className={"h-100"} justifyContent={"center"}>
                <MazeView/>
            </Grid2>
        </ThemeProvider>
    );
}

export default App;
