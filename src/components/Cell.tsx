import React, {useEffect, useState} from "react";
import {ICell} from "../maze-generator";
import {Box} from "@mui/material";
import {RadioButtonUnchecked, Adjust, AdbSharp} from "@mui/icons-material";

interface IProps {
    cell: ICell,
    pawnHere: boolean
}

export default function Cell(props: IProps) {

    const [cell, setCell] = useState<ICell | null>();

    useEffect(() => {
        setCell(props.cell);
    }, [props.cell]);

    const walls = props.cell.getWalls();

    return (
    (cell != null) ?
            <Box height={75} width={75} alignItems={"center"} display={"flex"} justifyContent={"center"} flexDirection={"column"}
                 bgcolor={(cell.getHighlighted()) ? 'green' : ''}
                borderTop={(walls.north) ? 1 : 0} borderBottom={(walls.south) ? 1 : 0} borderLeft={(walls.west) ? 1 : 0} borderRight={(walls.east) ? 1 : 0}>
                { (props.pawnHere) ? <AdbSharp fontSize={"large"} color={"primary"} style={{ fontSize: "3.5rem" }} /> : null }
                {
                        (cell.getMarkings() === 0) ? null
                            : (cell.getMarkings() === 1) ? <RadioButtonUnchecked fontSize={(props.pawnHere) ? "medium" :"large"}/>
                            : <Adjust color={"error"} fontSize={(props.pawnHere) ? "medium": "large"} />
                }
            </Box>
        : <React.Fragment />
)
}
