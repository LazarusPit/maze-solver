import React, {useState} from "react";
import {Menu} from "@mui/icons-material";
import {Box, Button, Dialog, DialogActions, DialogContent, DialogTitle, TextField} from "@mui/material";

interface IProps {
    onOpen: () => void,
    setColumns: (cols: number) => void,
    columns: number,
    rows: number,
    setRows: (rows: number) => void,
    delay: number,
    setDelay: (ms: number) => void
}

export default function ConfigDialog(props: IProps) {

    const [isOpen, setOpen] = useState(false);

    const handleClickOpen = () => {
        setOpen(true);
        props.onOpen();
    };

    const handleClose = () => {
        setOpen(false);
    };

    return (
        <React.Fragment>
            <Button style={{position: "fixed", backgroundColor: "black", color: "white"}}
                    onClick={handleClickOpen}><Menu style={{color: "white"}}/>Configure</Button>
            <Dialog open={isOpen} onClose={handleClose} PaperProps={{
                component: "form",
                onSubmit: (event: React.FormEvent<HTMLFormElement>) => {
                    event.preventDefault();
                    const formData = new FormData(event.currentTarget);
                    const formEntries = Object.fromEntries((formData.entries()));
                    const formEntriesAsInt = Object.keys(formEntries).reduce((finalObj, key) => {
                        const value = formEntries[key];
                        finalObj[key] = typeof value === "string" ? parseInt(value, 10) || 0 : 0;
                        return finalObj;
                    }, {} as Record<string, number>);
                    const {rows, cols, delay} = formEntriesAsInt;

                    if (props.rows != rows && rows > 0) {
                        props.setRows(rows);
                    }
                    if (props.columns != cols && cols > 0) {
                        props.setColumns(cols);
                    }
                    if (props.delay != delay && delay > 0) {
                        props.setDelay(delay);
                    }
                    handleClose();
                }
            }}>
                <DialogTitle>Configure</DialogTitle>
                <DialogContent>
                    <Box display={"flex"} marginY={1} gap={2}
                         flexDirection={"column"}
                         >
                    <TextField name={"rows"} fullWidth type={"number"}
                               label={"Rows"} defaultValue={props.rows}
                    />
                    <TextField name={"cols"} fullWidth type={"number"}
                               label={"Columns"} defaultValue={props.columns}
                    />
                    <TextField name={"delay"} fullWidth type={"number"} label={"Delay (ms)"} defaultValue={props.delay}
                    />
                    </Box>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color={"secondary"} variant={"outlined"}>Cancel</Button>
                    <Button type="submit" color={"primary"} variant={"contained"}>Confirm</Button>
                </DialogActions>
            </Dialog>
        </React.Fragment>
    )
}
