import React, {useEffect, useState} from "react";
import {Grid, mazeGenerator} from "../maze-generator";
import {Button, Grid2} from "@mui/material";
import {Pause, PlayArrow, RotateLeft, SkipPrevious} from "@mui/icons-material";
import CellView from "./Cell";
import {ICoord, coordFactory} from "../maze-generator/coord";
import ConfigDialog from "./ConfigDialog";
import Solver from "../solver";
import {Cell} from "../maze-generator/cell";

export default function MazeView() {


    const [rows, setRows] = useState(7);
    const [columns, setColumns] = useState(7);
    const [gridData, setGridData] = useState<string[][]>();
    const [position, setPosition] = useState<ICoord>(coordFactory(0, 0));
    const [positionInitial, setPositionInitial] = useState<ICoord>(coordFactory(0, 0));
    const [solver, setSolver] = useState<Solver>();
    const [solving, setSolving] = useState(false);
    const [delay, setDelay] = useState(250);
    const [isSolved, setIsSolved] = useState(false);

    useEffect(() => {
        setSolving(false);
        let grid = mazeGenerator({columns, rows}, "iterative");
        grid.carveExits(1);
        const randCoord = grid.getRandCoord();
        grid.highlightCell(randCoord, true);
        setPosition(randCoord);
        setPositionInitial(randCoord);
        setSolver(new Solver(grid, randCoord));
        setGridData(grid.toSerializable());
    }, [columns, rows]);

    useEffect(() => {
        if (solver !== undefined) {
            solver.setDelay(delay);
        }
    }, [delay, solver]);

    useEffect(() => {
        if (solving) {
            setIsSolved(false);
            solver?.start((position, finished, solved) => {
                setPosition(position);
                if (finished) {

                    gridData?.forEach((row) => {
                        console.info("[" + row.map((cellData) => Cell.newFromData(cellData).getMarkings()).join(", ") + "]");
                    });
                    setSolving(false);
                    if (solved) {
                        setIsSolved(true);
                    }
                }
            });
        } else {
            solver?.stop();
        }
    }, [solving]);

    const tracePath = () => {
        if (solver != null && gridData != undefined) {
            const shortestPath = solver.followShortestChartedPathTo(positionInitial);
            const grid = Grid.fromSerializable(gridData);
            shortestPath.forEach((cellCoord) => {
                grid.highlightCell(cellCoord, true);
            });
            setGridData(grid.toSerializable());
        }
    };

    const reset = () => {
        setSolving(false);
        setIsSolved(false);
        const savedRows = rows;
        const savedCols = columns;
        setRows(1);
        setColumns(1);
        setTimeout(() => {
            setRows(savedRows);
            setColumns(savedCols);
        }, 500);
    }

    const drawCellView = (rowIndex: number, colIndex: number, cellData: string | undefined) => {
        return (
            <CellView
                key={`${rowIndex}-${colIndex}`}
                cell={cellData ? Cell.newFromData(cellData) : new Cell(false)}
                pawnHere={position.row == rowIndex && position.col == colIndex}
            />
        );
    }

    return (
        (gridData != null) ?
            <React.Fragment>
                <ConfigDialog onOpen={() => setSolving(false)} columns={columns} rows={rows}
                              setColumns={(cols) => setColumns(cols)} setRows={(rows) => setRows(rows)} delay={delay}
                              setDelay={(delay) => setDelay(delay)}/>
                <div style={{position: "fixed", bottom: 0, border: "1px solid black"}}>
                    <Button onClick={() => setSolving(!solving)}>{(!solving) ? "Play" : "Pause"}
                        {(!solving) ? <PlayArrow fontSize={"large"}/> : <div><Pause fontSize={"large"}/><br/></div>}
                    </Button>
                    <Button onClick={reset}>Reset<RotateLeft fontSize={"large"}/></Button>
                    <Button disabled={!isSolved} onClick={tracePath}>Trace Path<SkipPrevious
                        fontSize={"large"}/></Button>
                </div>

                <Grid2 sx={{my: "2.5rem"}} justifyContent="center" alignContent="center">
                    {
                        [<Grid2 container key={-1} justifyContent="center">
                            {Array.from(Array(columns)).map((_, colIndex) => drawCellView(-1, colIndex, undefined))}
                        </Grid2>,
                            ...gridData.map((row, rowIndex) => {
                                return [
                                        <Grid2 container key={rowIndex} justifyContent="center">
                                            {row.map((cellData, colIndex) => {
                                                const cellView = drawCellView(rowIndex, colIndex, cellData);
                                                if (colIndex == 0) {
                                                    return [drawCellView(rowIndex, -1, undefined), cellView];
                                                }
                                                else if (colIndex == columns - 1) {
                                                    return [cellView, drawCellView(rowIndex, columns, undefined)];
                                                }
                                                return cellView;
                                            })}
                                        </Grid2>];
                                }
                            ),
                            <Grid2 container key={rows} justifyContent="center">
                                {Array.from(Array(columns)).map((_, colIndex) => drawCellView(rows, colIndex, undefined))}
                            </Grid2>]
                    }
                </Grid2>
            </React.Fragment>
            :
            <React.Fragment/>
    )
}
