export declare type Direction = 'north' | 'east' | 'south' | 'west';
export interface ICell {
    readonly getWalls: () => Record<Direction, boolean>;
    readonly getFreeDirections: () => Array<Direction>;
    readonly carveWall: (direction: Direction) => void;
    readonly buildWall: (direction: Direction) => void;
    readonly isWallSolid: (direction: Direction) => boolean;
    readonly isVisited: () => boolean;
    readonly leaveMarking: () => void;
    readonly getMarkings: () => number;
    readonly getOppositeDirection: (direction: Direction) => Direction;
    readonly getData: () => string;
    getHighlighted: () => boolean;
    readonly setHighlighted: (highlighted: boolean) => void;
}
export interface CellData {
    highlighted: boolean;
    visited: boolean;
    markings: 0|1|2;
    walls: Record<Direction, boolean>;
}
export class Cell implements ICell {

    private data: CellData;

    constructor(walled = true) {
        this.data = {
            highlighted: false,
            visited: false,
            markings: 0,
            walls: {
                north: walled,
                east: walled,
                west: walled,
                south: walled
            }
        }
    }

    getHighlighted(): boolean {
        return this.data.highlighted;
    }

    setHighlighted = (highlighted: boolean = true) => {
        this.data.highlighted = highlighted;
    }

    static newFromData(data: string) {
        const [north, east, south, west, markings] = data.split('');
        const cell = new Cell();
        if (north === '0')
            cell.carveWall('north');
        if (east === '0')
            cell.carveWall('east');
        if (west === '0')
            cell.carveWall('west');
        if (south === '0')
            cell.carveWall('south');
        if (!Number.isNaN(markings)) {
            let markingsInt = parseInt(markings);
            if (markingsInt < 3 && markingsInt > -1) {
                for (let i = 0; i < markingsInt; i++) {
                    cell.leaveMarking();
                }
            }
        }
        return cell;
    }

    getData() {
        const { walls: { north, east, south, west }, markings, highlighted } = this.data;
        const x = `${north ? 1 : 0}${east ? 1 : 0}${south ? 1 : 0}${west ? 1 : 0}${markings}${highlighted ? 1 : 0}`;
        const ref = cellCombinations.find(a => a === x);
        if (!ref) {
            throw new Error(`ref ${x} not found`);
        }
        return ref;
    }

    getWalls() {
        return this.data.walls;
    }

    carveWall(direction: Direction) {
        this.data.walls[direction] = false;
        this.data.visited = true;
    }

    buildWall(direction: Direction) {
        this.data.walls[direction] = true;
        this.data.visited = true;
    }

    leaveMarking() {
        if (this.data.markings < 2) {
            this.data.markings += 1;
        }
    }

    getMarkings() {
        return this.data.markings;
    };
    isWallSolid = (direction: Direction): boolean => this.data.walls[direction];
    isVisited = (): boolean => this.data.visited;

    getOppositeDirection(direction: Direction): Direction {
        if (direction === 'north') {
            return 'south';
        }
        else if (direction === 'east') {
            return 'west';
        }
        else if (direction === 'south') {
            return 'north';
        }
        return 'east';
    }

    getFreeDirections(): Array<Direction> {
        let walls = this.getWalls();
        let freeDirections: Direction[] = [];
        if (!walls.east) {
            freeDirections.push("east");
        }
        if (!walls.west) {
            freeDirections.push("west");
        }
        if (!walls.north) {
            freeDirections.push("north");
        }
        if (!walls.south) {
            freeDirections.push("south");
        }
        return freeDirections;
    }
}

// north east south west
// 1 = solid
// 0 = carved
const wallCombinations = [
    '0000',
    '0001',
    '0010',
    '0100',
    '1000',
    '1111',
    '0011',
    '0110',
    '1100',
    '1010',
    '0101',
    '1110',
    '0111',
    '1011',
    '1101',
    '1001'
];
const cellCombinations = [
    // markings: 0
    ...wallCombinations.map(w => `${w}0`),
    // markings: 1
    ...wallCombinations.map(w => `${w}1`),
    // markings: 2
    ...wallCombinations.map(w => `${w}2`)
].map(w => [`${w}0`, `${w}1`]).flatMap(x => x)