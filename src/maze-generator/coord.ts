export interface ICoord {
    readonly row: number;
    readonly col: number;
    readonly toString: () => string;
    readonly equalTo: (other: ICoord) => boolean;
}

class Coord implements ICoord {
    row: number;
    col: number;
    constructor(row: number, col: number) {
        this.toString = () => `[${row},${col}]`;
        this.row = row;
        this.col = col;
    }

    equalTo(other: ICoord): boolean {
        return this.row == other.row && this.col == other.col;
    }
}
export function coordFactory(row: number, col: number): ICoord {
    return new Coord(row, col);
}
