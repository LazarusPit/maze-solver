import {coordFactory, ICoord} from "./coord";
import {Direction, ICell, Cell} from "./cell";
import { randInRange } from './rand';

export interface IGrid {
	forEachRow: (cb: (row: Cell[], rowIndex: number) => void) => void;
	getCell: (coord: ICoord) => Cell | undefined;
	rows: number;
	cols: number;
	getExits: () => ICoord[];
	getAdjacentCell: (direction: Direction, coord: ICoord) => ICell | undefined;
	getAdjacentCoord: (direction: Direction, coord: ICoord) => ICoord | undefined;
	getRandCoord: () => ICoord;
	getRandCell: () => ICell;
	getAvailableCellWalls: (cell: ICell, cellCoord: ICoord, ignoreVisited: boolean) => Direction[];
	getCoordinatesOfNeighbouringCell: (coords: ICoord, direction: Direction) => ICoord;
	carveCellWall: (coord: ICoord, direction: Direction) => void;
	buildCellWall: (coord: ICoord, direction: Direction) => void;
	carveExits: (amount: number) => boolean;
	leaveCellMarking: (coord: ICoord) => void;
	highlightCell: (coord: ICoord, highlight: boolean) => IGrid;
    toSerializable: () => string[][];
}

export class Grid implements IGrid {

	public rows: number;
	public cols: number;
	private cells: string[][];
	private exits: ICoord[];

	constructor(rows: number, cols: number) {
		this.cells = [];
		this.rows = rows;
		this.cols = cols;
		this.exits = [];
		for ( let row = 0; row < rows; row++ ) {
			this.cells.push([]);
			for (let col = 0; col < cols; col++) {
				this.cells[row][col] = new Cell().getData();
			}
		}
	}

    static fromSerializable(data: string[][]): Grid {
        const rows = data.length;
        const cols = rows > 0 ? data[0].length : 0;
        const grid = new Grid(rows, cols);
        grid.cells = data;
        return grid;
    }

    toSerializable = (): string[][] => {
        return this.cells;
    }

	forEachRow = (cb: (row: Cell[], rowIndex: number) => void) => {
		this.cells.forEach((row, rowIndex) => {
			cb(row.map(Cell.newFromData), rowIndex);
		});
	};
	getCell = (coord: ICoord) => {
		const {row, col} = coord;
		const cells = this.cells;
		if ( row >= 0 && row < cells.length ) {
			const r = cells[row];
			if ( col >= 0 && col < r.length ) {
				return Cell.newFromData(r[col]);
			}
		}
	};
	getAdjacentCoord = (direction: Direction, {row, col}: ICoord) => {
		switch ( direction ) {
			case 'north':
				if ( row === 0 )
					return undefined;
				return coordFactory(row - 1, col);
			case 'east':
				if ( col === this.cols - 1 )
					return undefined;
				return coordFactory(row, col + 1);
			case 'south':
				if ( row === this.rows - 1 )
					return undefined;
				return coordFactory(row + 1, col);
			case 'west':
				if ( col === 0 )
					return undefined;
				return coordFactory(row, col - 1);
			default:
				return undefined;
		}
	}
	getAdjacentCell = (direction: Direction, coord: ICoord) => {
		const adjacentCoords = this.getAdjacentCoord(direction, coord);
		if ( ! adjacentCoords )
			return undefined;
		return this.coordInBounds(adjacentCoords)
			? this.getCell(adjacentCoords)
			: undefined;
	}
	getExits = () => this.exits;
	rowInBounds = (row: number) => row >= 0 && row < this.rows;
	colInBounds = (col: number) => col >= 0 && col < this.cols;
	coordInBounds = (coord: ICoord) => {
		return this.rowInBounds(coord.row) && this.colInBounds(coord.col);
	}
	getRandCoord = () => coordFactory(randInRange(0, this.rows - 1), randInRange(0, this.cols - 1));
	getRandCell = () => {
		const coord = this.getRandCoord();
		return Cell.newFromData(this.cells[coord.row][coord.col]);
	}
	isWallAvailable = (coord: ICoord, direction: Direction, cell: ICell, ignoreVisited: boolean = false) => {
		if ( cell.isWallSolid(direction) ) {
			const adjacentCell = this.getAdjacentCell(direction, coord);
			if (ignoreVisited) {
				return adjacentCell != null;
			}
			return adjacentCell && ! adjacentCell.isVisited();
		}
		return false;
	};
	getAvailableCellWalls = (cell: ICell, cellCoord: ICoord, ignoreVisited: boolean = false): Direction[] => {
		// available cell walls are walls that have not been carved and that are adjacent to a cell
		// that has not been visited
		const results: Direction[] = [];
		if ( this.isWallAvailable(cellCoord, 'north', cell, ignoreVisited) )
			results.push('north');
		if ( this.isWallAvailable(cellCoord, 'east', cell, ignoreVisited) )
			results.push('east');
		if ( this.isWallAvailable(cellCoord, 'south', cell, ignoreVisited) )
			results.push('south');
		if ( this.isWallAvailable(cellCoord, 'west', cell, ignoreVisited) )
			results.push('west');
		return results;
	};
	updateCell = ({row, col}: ICoord, cell: ICell) => {
		this.cells[row][col] = cell.getData();
	};
	carveCellWall = (coord: ICoord, direction: Direction) => {
		const cell = this.getCell(coord);
		if ( ! cell ) {
			throw new Error('cell not found');
		}
		cell.carveWall(direction);
		this.updateCell(coord, cell);
	};
	buildCellWall = (coord: ICoord, direction: Direction) => {
		const cell = this.getCell(coord);
		if (!cell) {
			throw new Error('cell not found');
		}
		cell.buildWall(direction);
		this.updateCell(coord, cell);
	}

	carveExits = (amount = 1) => {
		let exits = [];
		if ( amount > (this.rows * this.cols) || amount <= 0 ) {
			return false;
		}
		do {
			let row;
			let col;
			let direction: Direction;

			const carveFromRow = Math.random() >= 0.5;
			const carveFromFirstSide = Math.random() >= 0.5;
			if ( carveFromRow ) {
				col = randInRange(0, this.cols);
				// top
				if ( carveFromFirstSide ) {
					row = 0;
					direction = "north";
				}
				// bottom
				else {
					row = this.rows - 1;
					direction = "south";
				}

			} else {
				row = randInRange(0, this.rows);
				// left
				if ( carveFromFirstSide ) {
					col = 0;
					direction = "west";
				}
				// right
				else {
					col = this.cols - 1;
					direction = "east";
				}
			}
			exits.push({
				row, col, direction
			});
			amount--;
		} while ( amount > 0 );
		exits.forEach((exit) => {
			this.exits.push(this.getCoordinatesOfNeighbouringCell(coordFactory(exit.row, exit.col), exit.direction));
			this.carveCellWall(coordFactory(exit.row, exit.col), exit.direction);
		});
		return true;
	}

	leaveCellMarking = (coord: ICoord) => {
		const cell = this.getCell(coord);
		if ( ! cell ) {
			throw new Error('cell not found');
		}
		cell.leaveMarking();
		this.updateCell(coord, cell);
	}

	getCoordinatesOfNeighbouringCell(coords: ICoord, direction: Direction): ICoord {
		switch (direction) {
			case "north": return coordFactory(coords.row - 1, coords.col);
			case "south": return coordFactory(coords.row + 1, coords.col);
			case "east": return coordFactory(coords.row, coords.col + 1);
			case "west": return coordFactory(coords.row, coords.col - 1);
		}
	}

	highlightCell(coord: ICoord, highlight: boolean = true) {
		const cell = this.getCell(coord);
		if ( ! cell ) {
			throw new Error('cell not found');
		}
		cell.setHighlighted(highlight);
		this.updateCell(coord, cell);
        return this;
	}
}

export function gridFactory(rows: number, cols: number): IGrid {
	return new Grid(rows, cols);
}
