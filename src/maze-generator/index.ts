
import carveMaze, {Strategy} from './carve-maze';
import {IGrid} from "./grid";
interface Params {
    readonly rows: number;
    readonly columns: number;
}
export type { IGrid } from './grid';
export { Grid } from "./grid";
export type { ICell } from './cell';

export function mazeGenerator(params: Params, strategy: Strategy = 'iterative'): IGrid {
    const { rows, columns } = params;
    if (rows < 0) {
        throw new Error('rows must be a positive integer');
    }
    if (columns < 0) {
        throw new Error('columns must be a positive integer');
    }
    return carveMaze(rows, columns, strategy);
}
