import {IGrid} from "./maze-generator";
import {coordFactory, ICoord} from "./maze-generator/coord";
import {Cell, Direction, ICell} from "./maze-generator/cell";

declare type CellWithDirection = {
    cell: ICell,
    direction: Direction
}

export default class Solver {

    private grid: IGrid;
    private delay: number;
    private position: ICoord;
    private direction: Direction;
    private abort: Boolean;

    constructor(grid: IGrid, position: ICoord) {
        this.grid = grid;
        this.position = position;
        this.abort = false;
        this.direction = "north";
        this.delay = 100;
    }

    public getPosition(): ICoord {
        return this.position;
    }

    public async start(afterTurnCallback: (position: ICoord, finished: boolean, succeeded: boolean) => any = () => {
    }) {
        this.abort = false;
        do {
            afterTurnCallback(this.getPosition(), false, false);
            await this.sleep(this.delay);
            if (this.abort) {
                break;
            }
        } while (this.next());
        afterTurnCallback(this.getPosition(), true, !this.abort);
    }

    public stop() {
        this.abort = true;
    }

    public next(): boolean {
        const currentPosition = this.getPosition();
        let cell = this.grid.getCell(currentPosition);
        if (cell == undefined) {
            return false;
        }

        // don't mark intersections twice
        if (this.isIntersection(cell) === false || this.isCorner(cell)) {
            this.grid.leaveCellMarking(currentPosition);
        } else if (cell.getMarkings() < 1) {
            this.grid.leaveCellMarking(currentPosition);
        }

        let availableDirections = cell.getFreeDirections();
        console.log("Available directions", availableDirections);

        let adjacentCells: CellWithDirection[] = [];

        for (const cellDirection of availableDirections) {
            let adjacentCell = this.grid.getAdjacentCell(cellDirection, currentPosition);
            if (adjacentCell) {
                adjacentCells.push({cell: adjacentCell, direction: cellDirection});
            } else {
                adjacentCells.push({cell: new Cell(), direction: cellDirection});
            }
        }

        adjacentCells = adjacentCells.filter((data) => this.isBlocked(data.cell) === false);

        adjacentCells.sort((aData, bData) => aData.cell.getMarkings() - bData.cell.getMarkings());
        if (adjacentCells.length === 0) {
            return false;
        } else if (adjacentCells.length === 1) {
            let adjacentCell = adjacentCells[0];
            this.direction = adjacentCell.direction;
        }
        // adjacentCells.length > 1
        else {
            if (adjacentCells[0].cell.getMarkings() === 0) {

                if (adjacentCells[adjacentCells.length - 1].cell.getMarkings() === 0) {
                    // all cells: markings = 0
                    if (this.direction === undefined || adjacentCells.filter((data) => data.direction === this.direction).length < 1) {
                        this.direction = adjacentCells[Math.floor(Math.random() * adjacentCells.length)].direction;
                    }
                } else {
                    // last cell: markings = 1
                    adjacentCells = adjacentCells.filter((data) => data.cell.getMarkings() < adjacentCells[adjacentCells.length - 1].cell.getMarkings());
                    this.direction = adjacentCells[Math.floor(Math.random() * adjacentCells.length)].direction;
                }
            }
            // all remaining cells are marked once
            else {
                console.log(this.direction);
                console.log("All surrounding cells marked once:", adjacentCells);
                // in case the previous direction hasn't been set or it's obstructed
                if (this.direction === undefined || adjacentCells.filter((data) => data.direction === this.direction).length < 1) {
                    adjacentCells = adjacentCells.filter((data) => data.direction !== cell!.getOppositeDirection(this.direction));
                    let nonIntersections = adjacentCells.filter((data) => this.isIntersection(data.cell) === false);

                    if (cell.getMarkings() === 2) {
                        if (nonIntersections.length > 0) {
                            this.direction = nonIntersections[Math.floor(Math.random() * nonIntersections.length)].direction;
                        } else if (adjacentCells.length > 0) {
                            this.direction = adjacentCells[Math.floor(Math.random() * adjacentCells.length)].direction;
                        }
                    } else {
                        this.direction = adjacentCells[Math.floor(Math.random() * adjacentCells.length)].direction;
                    }
                }
            }
        }
        console.log("Moving", this.direction);
        this.position = this.grid.getCoordinatesOfNeighbouringCell(currentPosition, this.direction);

        return !this.grid.getExits().includes(this.position);
    }

    public isIntersection(cell: ICell): Boolean {
        return cell.getFreeDirections().length > 2;
    }

    public isCorner(cell: ICell): Boolean {
        let freeDirections = cell.getFreeDirections();
        if (freeDirections.length >= 2) {
            if (freeDirections.length === 3) return false;
            return cell.getOppositeDirection(freeDirections[0]) !== freeDirections[1];
        }
        return false;
    }

    public isBlocked(cell: ICell): Boolean {
        return cell.getMarkings() === 2;
    }

    public setDelay(milliseconds: number) {
        this.delay = milliseconds;
    }

    public followShortestChartedPathTo(cellDestination: ICoord) {
        const positionCopiedByValue = this.position;
        console.info(coordFactory(1, 1).equalTo(coordFactory(1, 1)));
        return this.recursiveRetrace(positionCopiedByValue, cellDestination, []);
    }


    private recursiveRetrace(cursorCoords: ICoord, destinationCoords: ICoord, pathCharted: ICoord[] = []): ICoord[] {
        if (cursorCoords.equalTo(destinationCoords)) return pathCharted;
        let currentCell = this.grid.getCell(cursorCoords);
        // TODO: Fill stack and compare distance (rejection of dead-ends)
        if (currentCell === undefined) {
            console.log("Position is out of bounds", cursorCoords);
            // outside the grid
            if (cursorCoords.row < 0) {
                cursorCoords = {...cursorCoords, row: 0};
            } else if (cursorCoords.row > this.grid.rows - 1) {
                cursorCoords = {...cursorCoords, row: this.grid.rows - 1};
            }
            if (cursorCoords.col < 0) {
                cursorCoords = {...cursorCoords, col: 0}
            } else if (cursorCoords.col > this.grid.cols - 1) {
                cursorCoords = {...cursorCoords, col: this.grid.cols - 1}
            }
            if (pathCharted.includes(cursorCoords)) {
                return [];
            }
            console.info("Reset to ", cursorCoords);
            currentCell = this.grid.getCell(cursorCoords);
            if (currentCell === undefined) return [];
        }

        const availableDirections = currentCell.getFreeDirections()
            .filter((direction) => {
                const neighbouringCoordinates = this.grid.getCoordinatesOfNeighbouringCell(cursorCoords, direction);
                const neighbouringCell = this.grid.getCell(neighbouringCoordinates);
                if (neighbouringCell) {
                    return [1, 2].includes(neighbouringCell.getMarkings());
                }
                return false;
            });

        return availableDirections.map((direction) => {
            const coordinatesOfNeighbour = this.grid.getCoordinatesOfNeighbouringCell(cursorCoords, direction);
            if (pathCharted.find(element => coordinatesOfNeighbour.equalTo(element)) != undefined) {
                return [];
            }
            const pathChartedWithNextStep = [...pathCharted, coordinatesOfNeighbour];
            return this.recursiveRetrace(coordinatesOfNeighbour, destinationCoords, pathChartedWithNextStep);
        }).reduce((saved, pathUnderConsideration) => (pathUnderConsideration.length == 0 || (saved.length != 0 && saved.length < pathUnderConsideration.length)) ? saved : pathUnderConsideration, []);
    }

    private async sleep(ms: number) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }

}
